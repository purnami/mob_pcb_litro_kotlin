package com.example.lib.helper

import android.content.Context

class APDUHelper() {
    private val TAG = "APDUHelper"

    var length = 0
//    var ctx: Context? = null
    val errorCode=ErrorCode()

//    constructor() {
//        this.ctx = ctx
//    }

    fun CheckResult(APDU: ByteArray?): Int {
        var bResult: Int = errorCode.OK
        if (APDU == null) {
            bResult = errorCode.ERR_NO_RESP
            return bResult
        }
        if (APDU.size <= 1) {
            bResult = errorCode.ERR_NO_RESP
            return bResult
        }
        if (APDU[APDU.size - 2] == 0X6E.toByte() && APDU[APDU.size - 1] == 0X00.toByte()) {
            bResult = errorCode.ERR_NODATA
            return bResult
        }
        if (APDU[APDU.size - 2] == 0X6A.toByte() && APDU[APDU.size - 1] == 0X86.toByte()) {
            bResult = errorCode.ERR_NODATA
            return bResult
        }
        if (APDU[APDU.size - 2] == 0X6A.toByte() && APDU[APDU.size - 1] == 0X83.toByte()) {
            bResult = errorCode.ERR_NODATA
            return bResult
        }
        if (APDU[APDU.size - 2] == 0X92.toByte() && APDU[APDU.size - 1] == 0X01.toByte()) {
            //bResult=TOPUP_EXCEEDS_MAXBALANCE;
            return bResult
        }
        if (APDU[APDU.size - 2] == 0X92.toByte() && APDU[APDU.size - 1] == 0X00.toByte()) {
            bResult = errorCode.CTL_ERR_INSUFFICIENTBALANCE
            return bResult
        }
        if (APDU[APDU.size - 2] == 0X6A.toByte() && APDU[APDU.size - 1] == 0X82.toByte()) {
            bResult = errorCode.CTL_ERR_UNKNOWNCARD
            return bResult
        }
        if (APDU[APDU.size - 2] == 0X61.toByte()) {
            bResult = errorCode.DATA_AVAILABLE
            return bResult ///ok
        }
        if (APDU[APDU.size - 2] == 0X6C.toByte()) {
            bResult = errorCode.DATA_AVAILABLE
            return bResult ///ok
        }
        //desfire
        if (APDU[APDU.size - 2] == 0X67.toByte() && APDU[APDU.size - 1] == 0X00.toByte()) {
            bResult = errorCode.ERR_LENGTH
            return bResult ///ok
        }
        //desfire
        if (APDU[APDU.size - 2] == 0X91.toByte() && APDU[APDU.size - 1] == 0X00.toByte()) {
            return bResult ///ok
        }

        //Mega
        if (APDU[APDU.size - 2] == 0X91.toByte() && APDU[APDU.size - 1] == 0XAF.toByte()) {
            return bResult ///ok
        }
        if (APDU[APDU.size - 2] == 0X90.toByte() && APDU[APDU.size - 1] == 0XAF.toByte()) {
            return bResult ///ok
        }

        //LuminOS
        if (APDU[APDU.size - 2] == 0X69.toByte() && APDU[APDU.size - 1] == 0X85.toByte()) {
            bResult = errorCode.ERR_NODATA
            return bResult ///ok
        }
        if (!(APDU[APDU.size - 2] == 0X90.toByte() && APDU[APDU.size - 1] == 0X00.toByte())) {
            bResult = errorCode.ERR_UNKNOWN
            return bResult
        }
        return bResult
    }

}