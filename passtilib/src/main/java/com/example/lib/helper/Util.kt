package com.example.lib.helper

import java.text.SimpleDateFormat
import java.util.*

class Util() {
    fun dateNow(dateFormat: String?): String? {
        val cal = Calendar.getInstance()
        val sdf = SimpleDateFormat(dateFormat)
        return sdf.format(cal.time)
    }
}