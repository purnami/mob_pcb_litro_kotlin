package com.example.lib.helper

import com.hoho.android.usbserial.driver.UsbSerialPort
import java.io.ByteArrayOutputStream
import kotlin.experimental.and

class Hex() {
    fun byteToHexString(b: Byte): String? {
        val n = (b and 0x000000FF.toByte()).toInt()
        val result = (if (n < 0x00000010) "0" else "") + Integer.toHexString(n)
        return result.toUpperCase()
    }

    fun hexStringToByteArray(s: String): ByteArray? {
        val len = s.length
        val data = ByteArray(len / 2)
        var i = 0
        while (i < len) {
            data[i / 2] =
                ((Character.digit(s[i], 16) shl 4) + Character.digit(s[i + 1], 16)).toByte()
            i += 2
        }
        return data
    }

    fun bytesToHexString(bArray: ByteArray?): String? {
        if (bArray == null) {
            return null
        }
        val sb = StringBuffer(bArray.size)
        var sTemp: String
        var j = 0
        for (i in bArray.indices) {
            sTemp = Integer.toHexString(0xFF and bArray[i].toInt())
            if (sTemp.length < 2) sb.append(0)
            sb.append(sTemp.toUpperCase())
            j++
        }
        return sb.toString()
    }

    fun sum(array: ByteArray): Int {
        var result = 0
        for (v in array) {
            result += v.toInt()
        }
        return result
    }

    fun decodeL(data: ByteArray): String? {
        try {
            return decodeByte(data)?.toString(Charsets.UTF_8)
        } catch (e: Exception) {
            return null
        }
    }

    fun encodeL(data: String): ByteArray? {
        var bos = ByteArrayOutputStream()
        var ba = data.toByteArray(Charsets.UTF_8)
        bos.write(ba)
        var cursum = ba.sum() % 256
        val h = (256 - cursum)
        bos.write(byteArrayOf(h.toByte()))
        bos.write(0b0)
        bos.close()
        return bos.toByteArray()
    }

    fun doWrite(bytes: ByteArray, port: UsbSerialPort): ByteArray? {
        try {
//            if (bytes.last() != 0b0.toByte()) throw CTLReaderException("NOT VALID")
            var t = port?.write(bytes, bytes.size * 50)
            Thread.sleep(500)
            val retBa = ByteArray(128)
            val len = port?.read(retBa, 128 * 50)
            var ret = retBa.copyOfRange(0, len ?: 0)
//            logbytes("OTG SEND", bytes, ret)
            return ret
        } catch (e: Exception) {
            return null
            throw e
//            logbytes("OTG ERROR", bytes, null, e.message.toString())
//            throw CTLReaderException(e.message.toString())
        }
    }

    fun decodeByte(data: ByteArray): ByteArray? {
        try {
            if (data.isEmpty()) {
                return null
            }
            var cursum = 0
            for (b in 0..data.size - 3) {
                cursum += data.get(b)
            }
            var sum = flipbits(data.get(data.size - 2) - 1)
//            Log.d("check sum", cursum.toString() + " and " + sum.toString())
            var bo = data.inputStream(0, data.size - 2)
            return bo.readBytes()
        } catch (e: Exception) {
            return null
        }
    }

    fun flipbits(i: Int): Int {
        var bitString = Integer.toBinaryString(i)
        if (bitString.length % 4 != 0) {
            bitString = "0000$bitString".substring(bitString.length % 4)
        }
        for (io in 0 until bitString.length - 1) {
            val stringBuilder = StringBuilder(bitString)
            stringBuilder.setCharAt(io, (if (stringBuilder[io] == '0') '1' else '0'))
            bitString = stringBuilder.toString()
        }
        return bitString.toInt(2)
    }
}