package com.example.lib.passti

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbDeviceConnection
import android.hardware.usb.UsbManager
import android.nfc.NfcAdapter
import android.nfc.NfcManager
import android.os.Parcelable
import android.util.Log
import com.example.lib.helper.ErrorCode
import com.example.lib.helper.Hex
import com.hoho.android.usbserial.driver.*
import java.io.IOException
import java.math.BigInteger

class Usb {
    var usbManager: UsbManager? = null
    var usbDevice: UsbDevice? = null
    var ctx: Context? = null
    var intent: Intent? = null
    var customTable: ProbeTable? = null
    var permissionIntent: PendingIntent? = null

    private var nfcAdapter: NfcAdapter? = null

    private val ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION"
    private val LUMINOS_AID = "A000000571504805"
    private val APDU_SELECT_FILE = "00A4040008"
    private val APDU_READ_BALANCE = "904C000004"

    private var modeOtg = false
    var port: UsbSerialPort? = null
    var readidContinue = false

    private var connection: UsbDeviceConnection? = null
    private var usbSerialPort: UsbSerialPort? = null

    val errorCode=ErrorCode()
    val hex=Hex()

    constructor(ctx: Context?) {
        this.ctx = ctx
    }

    fun getConnection(): UsbDeviceConnection? {
        return connection
    }

    fun setConnection(connection: UsbDeviceConnection?) {
        this.connection = connection
    }

    fun getUsbSerialPort(): UsbSerialPort? {
        return usbSerialPort
    }

    fun setUsbSerialPort(usbSerialPort: UsbSerialPort?) {
        this.usbSerialPort = usbSerialPort
    }

//    fun Usb(ctx: Context?) {
//        this.ctx = ctx
////        this.intent=intent;
//    }



    fun initNfc() {
        nfcAdapter = (ctx!!.getSystemService(Context.NFC_SERVICE) as NfcManager).defaultAdapter
        if (nfcAdapter == null) {
            modeOtg = true
            return
        }
    }

    fun init(): Int {
        Log.d("masuk", "2")
        //        nfcAdapter=((NfcManager) ctx.getSystemService(Context.NFC_SERVICE)).getDefaultAdapter();
//        if (nfcAdapter == null) {
//            modeOtg = true;
//        }
        if (usbManager == null) {
            usbManager = ctx!!.getSystemService(Context.USB_SERVICE) as UsbManager
        }
        if (permissionIntent == null) {
            permissionIntent = PendingIntent.getBroadcast(ctx, 0, Intent(ACTION_USB_PERMISSION), 0)
            val filter = IntentFilter(ACTION_USB_PERMISSION)
            ctx!!.registerReceiver(usbReceiver, filter)
        }
        customTable = ProbeTable()
        customTable!!.addProduct(1241, 46388, CdcAcmSerialDriver::class.java)
        val prober = UsbSerialProber(customTable)
        val drivers = prober.findAllDrivers(usbManager)
        if (drivers.isEmpty()) {
            return -1
        }
        val driver = drivers[0]
        if (usbDevice == null) {
            usbDevice = driver.device
            Log.d("vendorId", "" + driver.device.vendorId)
            Log.d("productId", "" + driver.device.productId)
        }
        if (!usbManager!!.hasPermission(usbDevice)) {
            Log.d("masukpermission", "1")
            usbManager!!.requestPermission(usbDevice, permissionIntent)
            //            return;
        }
        Log.d("masukopendevice", "1")
        connection = usbManager!!.openDevice(usbDevice)
        setConnection(connection)
        if (connection == null) {
//            nfcReaderSync = null;
            port = null
            //            return;
        }
        usbSerialPort = driver.ports[0]
        setUsbSerialPort(usbSerialPort)

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                setupPort(usbSerialPort, connection);
//                Log.d("masukcekbal", "1");
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        if(modeOtg&&!readidContinue){
//                            readid();
//                        }
//                    }
//                }).start();
//            }
//        }).start();
//        return;
        return errorCode.OK
    }


    private fun readid() {
        readidContinue = true
        Thread { readIdLoop() }.start()
    }

    @Synchronized
    private fun readIdLoop() {
        Log.d("readmasuk", "2")
        val ss: String = hex.decodeL(hex.doWrite(hex.encodeL("40")!!, port!!)!!)!!
        Log.d("sss", "" + ss)
        if (ss != null && ss.contains("9000") && ss.length >= 4) {
            Log.d("readmasuk", "3")
            readidContinue = false
            checkBal()
        }
        if (readidContinue) {
            Log.d("readmasuk", "4")
            try {
                Thread.sleep(200)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            readIdLoop()
        }
    }

    private fun checkBal() {
        Thread {
            Log.d("modeotgg", "" + modeOtg)
            val selectfile = transceiveNFC(APDU_SELECT_FILE + LUMINOS_AID, 4, 0)
            Log.d("selectfile", "" + selectfile)
            val balance = transceiveNFC(APDU_READ_BALANCE, 4, 0)
            Log.d("balance", "" + balance)
            val `in`: BigInteger = BigInteger(hex.hexStringToByteArray(balance!!))
            Log.d("balance2", "" + `in`.toString())
        }.start()
    }

    private fun transceive(input: ByteArray): String? {
        Log.d("inputtransceive", "" + hex.bytesToHexString(input))
        Log.d("masukkedecode", "1")
        return hex.decodeL(hex.doWrite(input, port!!)!!)
    }

    private fun transceiveNFC(input: String, minLength: Int, currentLoop: Int): String? {
        Log.d("inputt", "" + input)
        var ret: String? = null
        Log.d("masuknfc", "1")
        if (modeOtg) {
            ret = transceive(hex.encodeL("30" + input.trim { it <= ' ' })!!)
            println("rett $ret")
            Log.d("rett", "" + ret)
            if (ret != null && ret.endsWith("9000")) {
                ret = ret.substring(0, ret.length - 4)
            }
        }
        if (ret != null && ret.length >= minLength) {
            Log.d("tesmasuk", "5")
            Log.d("final ret1", "" + ret)
            return ret
        }
        return ret
    }

    private fun setupPort(usbSerialPort: UsbSerialPort, connection: UsbDeviceConnection) {
        Thread {
            port = usbSerialPort
            try {
                port!!.open(connection)
                port!!.setParameters(
                    38400,
                    UsbSerialPort.DATABITS_8,
                    UsbSerialPort.STOPBITS_1,
                    UsbSerialPort.PARITY_NONE
                )
                val a: ByteArray = hex.encodeL("10")!!
                Log.d("encodel1", "" + hex.bytesToHexString(a))
                val b: ByteArray = hex.doWrite(a, port!!)!!
                Log.d("dowrite1", "" + hex.bytesToHexString(b))
                val c: String = hex.decodeL(b)!!
                Log.d("decodel1", "" + c + "  " + hex.decodeL(b))
                if (!c.contains("9000")) {
                    Log.d("masuk error", "1")
                    throw IOException("INIT NFC FAILED")
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }.start()
    }

    private val usbReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (ACTION_USB_PERMISSION == intent.action) {
                synchronized(this) {
                    val device: UsbDevice? = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE)
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        device?.apply {
                            //call method to set up device communication
                            usbDevice = device
                            Log.d("Permission allowed", "" + device)
                            if (modeOtg) init()
                        }
                    } else {
                        Log.d("Permission denied", "" + device)
                    }
                }
            }
        }
    }

//    private val usbReceiver: BroadcastReceiver = object : BroadcastReceiver() {
//        override fun onReceive(context: Context, intent: Intent) {
//            if (ACTION_USB_PERMISSION === intent.action) {
//                synchronized(ctx!!) {
//                    val device =
//                        intent.getParcelableExtra<Parcelable>(UsbManager.EXTRA_DEVICE) as UsbDevice?
//                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
//                        if (device != null) {
//                            usbDevice = device
//                            Log.d("Permission allowed", "" + device)
//                            if (modeOtg) init()
//                        }
//                    } else {
//                        Log.d("Permission denied", "" + device)
//                    }
//                }
//            }
//        }
//    }
}