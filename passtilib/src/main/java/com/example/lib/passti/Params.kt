package com.example.lib.passti

import android.content.Context

class Params {
    val NORMAL: Byte = 0 /*transaction run in normal*/
    val REPURCHASE: Byte = 1 /*lost contact during purchase process*/
    val REWRITEPARTNERDATA: Byte = 2 /*lost contact during writing partner data*/


    val ctype_unknown: Int = 0
    val ctype_luminos: Int = 1
    val ctype_mdr: Int = 2
    val ctype_bri: Int = 3
    val ctype_bni: Int = 4
    val ctype_bca: Int = 5
    val ctype_dki: Int = 6
    val ctype_nobu: Int = 7
    val ctype_mega: Int = 8


    val UNKNOWN: Byte = 0
    val DEV_JELLIES: Byte = 2
    val DEV_APOS: Byte = 3
    val DEV_SUNMI: Byte = 7
    val DEV_NEWLAND: Byte = 10
    val DEV_WIZAR: Byte = 9
    val DEV_PAX: Byte = 10
    val DEV_MOREFUN: Byte = 11


    var cardtype: Byte = 0
    var prevcardtype: Byte = 0
    var deductAction: Int=0


    lateinit var prevCardNo: ByteArray
    lateinit var prevUID: ByteArray
    var fSaveSaldo = false
    lateinit var prevAmt: ByteArray

    lateinit var prevTRH: ByteArray
    var LastlBalanceKartu: Long = 0
    lateinit var prevBalance: ByteArray
    lateinit var ExpctBalance: ByteArray
    lateinit var baAmount: ByteArray


    constructor() {
        cardtype = 0
        prevcardtype = 0
        deductAction =
            NORMAL.toInt() //describe deduct action : NORMAL,REPURCHASE,REWRITEPARTNERDATA
        prevCardNo = ByteArray(8)
        prevUID = ByteArray(7)
        fSaveSaldo = false
        prevAmt = ByteArray(4)
        prevTRH = ByteArray(8)
        LastlBalanceKartu = 0
        prevBalance = ByteArray(4)
        ExpctBalance = ByteArray(4)
        baAmount = ByteArray(4)
    }
//    fun Params() {
//        cardtype = 0
//        prevcardtype = 0
//        deductAction =
//            NORMAL.toInt() //describe deduct action : NORMAL,REPURCHASE,REWRITEPARTNERDATA
//        prevCardNo = ByteArray(8)
//        prevUID = ByteArray(7)
//        fSaveSaldo = false
//        prevAmt = ByteArray(4)
//        prevTRH = ByteArray(8)
//        LastlBalanceKartu = 0
//        prevBalance = ByteArray(4)
//        ExpctBalance = ByteArray(4)
//        baAmount = ByteArray(4)
//    }
}