package com.example.lib.passti

import android.content.Context
import android.content.Intent
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.util.Log
import com.example.lib.helper.ErrorCode
import com.hoho.android.usbserial.driver.ProbeTable

class Reader {
    var usbManager: UsbManager? = null
    var usbDevice: UsbDevice? = null
    var ctx: Context? = null
    var customTable: ProbeTable? = null
    var usb: Usb? = null
    var contactless: Contactless? = null
    var sam: Sam? = null
    var intent: Intent? = null
    val errorCode=ErrorCode()

    constructor(ctx: Context?) {
        this.ctx = ctx
        usb = Usb(ctx)
        contactless = Contactless(ctx)
        sam = Sam(ctx)
    }

//    fun Reader(ctx: Context?) {
//        this.ctx = ctx
//        usb = Usb(ctx)
//        contactless = Contactless(ctx)
//        sam = Sam(ctx)
//    }



    fun init(): Int {
        Log.d("masuk", "1")
        //        nfc.init();
        var result: Int = errorCode.NOT_OK
        result = usb!!.init()
        Log.d("resultinitusb", "" + result)
        if (result != errorCode.OK) {
            return result
        }
        Log.d("masukctl", "1")
        result = contactless!!.init(usb!!.getUsbSerialPort(), usb!!.getConnection())
        Log.d("resultinitctl", "" + result)
        if (result != errorCode.OK) {
            return result
        }
        result = sam!!.init(usb!!.getUsbSerialPort(), usb!!.getConnection())
        Log.d("resultinitsam", "" + result)
        return if (result != errorCode.OK) {
            result
        } else errorCode.OK
    }

    fun cekBalance(): Int {
        var result: Int = errorCode.NOT_OK
        result = contactless!!.cekBalance()
        return result
    }

    fun deduct(): Int {
        var result: Int = errorCode.NOT_OK
        result = sam!!.deduct()
        return result
    }

    fun sendCTL(apdu: String?): ByteArray? {
        return contactless!!.sendCTL(apdu!!)
    }

    fun sendSAM(apdu: String?): ByteArray? {
        return sam!!.sendSAM(apdu!!)
    }
}