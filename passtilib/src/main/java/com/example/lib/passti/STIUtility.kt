package com.example.lib.passti

import android.content.Context
import android.util.Log
import com.example.lib.card.BNI
import com.example.lib.card.BRI
import com.example.lib.card.Luminos
import com.example.lib.card.Mandiri
import com.example.lib.helper.ErrorCode

class STIUtility {
    var ctx: Context? = null
    var reader: Reader? = null
    var luminos: Luminos? = null
    var bri: BRI? = null
    var bni: BNI? = null
    var mandiri: Mandiri? = null

    var card = 0
    var saldoAwal = 0
    var saldoAkhir:Int = 0

    val errorCode=ErrorCode()
    val params= Params()

    constructor(ctx: Context?) {
        this.ctx = ctx
        reader = Reader(ctx)
    }


//    fun STIUtility(ctx: Context) {
//        this.ctx = ctx
//        reader = Reader(ctx)
//    }

    fun initReader(): Int {
        return reader!!.init()
    }

    fun cekBalance(): Int {
        var i: Int = errorCode.ERR_CARDTYPEINACTIVE
        Log.d("masukcekbalance", "1")
        card = checkMyCard()
        Log.d("carddcheck", "" + card)
        when (card) {
            params.ctype_luminos -> i = luminos!!.cekBalance()
            params.ctype_mdr -> i = mandiri!!.cekBalance()
            params.ctype_bri -> {
                Log.d("cekbalancebri", "1")
                i = bri!!.cekBalance()
            }
            params.ctype_bni -> i = bni!!.cekBalance()
        }
        return i
//        return 0;
//        return reader.cekBalance();
    }

    private fun checkMyCard(): Int {
        Log.d("masukcheckmycard", "1")
        var i: Int = params.ctype_unknown
        Log.d("cardcheckmycard", "" + card)
        when (card) {
            1 -> {
                i = luminos!!.CTL_SelectApp()
                Log.d("luminos", "" + i)
                if (i == errorCode.OK) {
                    return params.ctype_luminos
                }
            }
            2 -> {
                i = mandiri!!.CTL_Init()
                Log.d("mandiri", "" + i)
                if (i == errorCode.OK) {
                    return params.ctype_mdr
                }
            }
            3 -> {
                i = bri!!.CTL_SelectAIDDesfire()
                Log.d("brii", "" + i)
                if (i == errorCode.OK) {
                    return params.ctype_bri
                }
            }
            4 -> {
                i = bni!!.CTL_SelectAID()
                Log.d("bnii", "" + i)
                if (i == errorCode.OK) {
                    return params.ctype_bni
                }
            }
        }
        return params.ctype_unknown
    }

    fun deduct(amount: Int): Int {
        Log.d("nominal", "" + amount)
        var i: Int = errorCode.ERR_CARDTYPEINACTIVE
        val card = checkMyCard()
        Log.d("cardd", "" + card)
        when (card) {
            params.ctype_luminos -> i = luminos!!.deduct(amount)
        }
        return i
//        return reader.deduct();
    }

    fun initPower(card: Int): Int {
        when (card) {
            1 -> {
                luminos = Luminos(ctx, reader)
                this.card = params.ctype_luminos
            }
            2 -> {
                mandiri = Mandiri(ctx, reader)
                this.card = params.ctype_mdr
            }
            3 -> {
                bri = BRI(ctx, reader)
                this.card = params.ctype_bri
            }
            4 -> {
                bni = BNI(ctx, reader)
                this.card = params.ctype_bni
            }
        }
        return 0
    }

    fun getBalance(): Int {
        when (card) {
            1 -> saldoAwal = luminos!!.getBalance()
            2 -> saldoAwal = mandiri!!.getBalance()
            4 -> saldoAwal = bni!!.getBalance()
        }
        return saldoAwal
    }

    fun getDeduct(): Int {
        when (card) {
            1 -> saldoAkhir = luminos!!.getDeduct()
        }
        return saldoAkhir
    }
}