package com.example.lib.passti

import android.content.Context
import android.hardware.usb.UsbDeviceConnection
import android.nfc.NfcAdapter
import android.nfc.NfcManager
import android.util.Log
import com.example.lib.helper.Hex
import com.hoho.android.usbserial.driver.UsbSerialPort
import java.io.IOException
import java.math.BigInteger

class Contactless {
    var ctx: Context? = null
    var port: UsbSerialPort? = null
    var connection: UsbDeviceConnection? = null
    private var modeOtg = false
    private var nfcAdapter: NfcAdapter? = null
    var usb: Usb? = null
    var readidContinue = false
    val hex=Hex()

    private val LUMINOS_AID = "A000000571504805"
    private val APDU_SELECT_FILE = "00A4040008"
    private val APDU_READ_BALANCE = "904C000004"

    constructor(ctx: Context?) {
        this.ctx = ctx
    }

//    fun Contactless(ctx: Context?) {
//        this.ctx = ctx
//    }



    fun init(usbSerialPort: UsbSerialPort?, connection: UsbDeviceConnection?): Int {
        nfcAdapter = (ctx!!.getSystemService(Context.NFC_SERVICE) as NfcManager).defaultAdapter
        if (nfcAdapter == null) {
            modeOtg = true
        }
        port = usbSerialPort
        this.connection = connection
        setupPort(port, this.connection)
        return 0
    }

    private fun readid() {
        readidContinue = true
        Thread { readIdLoop() }.start()
    }

    @Synchronized
    private fun readIdLoop() {
        Log.d("readmasuk", "2")
        val ss: String = hex.decodeL(hex.doWrite(hex.encodeL("40")!!, port!!)!!)!!
        Log.d("sss", "" + ss)
        if (ss != null && ss.contains("9000") && ss.length >= 4) {
            Log.d("readmasuk", "3")
            readidContinue = false
            checkBal()
        }
        if (readidContinue) {
            Log.d("readmasuk", "4")
            try {
                Thread.sleep(200)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            readIdLoop()
        }
    }

    private fun checkBal() {
        Thread {
            Log.d("modeotgg", "" + modeOtg)
            val selectfile = transceiveNFC(APDU_SELECT_FILE + LUMINOS_AID, 4, 0)
            Log.d("selectfile", "" + selectfile)
            val balance = transceiveNFC(APDU_READ_BALANCE, 4, 0)
            Log.d("balance", "" + balance)
            val `in`: BigInteger = BigInteger(hex.hexStringToByteArray(balance!!))
            Log.d("balance2", "" + `in`.toString())
        }.start()
    }

    private fun transceive(input: ByteArray): String {
        Log.d("inputtransceive", "" + hex.bytesToHexString(input))
        Log.d("masukkedecode", "1")
        return hex.decodeL(hex.doWrite(input, port!!)!!)!!
    }

    private fun transceiveNFC(input: String, minLength: Int, currentLoop: Int): String? {
        Log.d("inputt", "" + input)
        var ret: String? = null
        Log.d("masuknfc", "1")
        if (modeOtg) {
            ret = transceive(hex.encodeL("30" + input.trim { it <= ' ' })!!) //transceive
            println("rett $ret")
            Log.d("rett", "" + ret)
            if (ret != null && ret.endsWith("9000")) {
                ret = ret.substring(0, ret.length - 4)
            }
        }
        if (ret != null && ret.length >= minLength) {
            Log.d("tesmasuk", "5")
            Log.d("final ret1", "" + ret)
            return ret
        }
        return ret
    }

    private fun setupPort(usbSerialPort: UsbSerialPort?, connection: UsbDeviceConnection?) {
        Thread {
            port = usbSerialPort
            try {
                port!!.open(connection)
                port!!.setParameters(
                    38400,
                    UsbSerialPort.DATABITS_8,
                    UsbSerialPort.STOPBITS_1,
                    UsbSerialPort.PARITY_NONE
                )
                val a: ByteArray = hex.encodeL("10")!!
                Log.d("encodel1", "" + hex.bytesToHexString(a))
                val b: ByteArray = hex.doWrite(a, port!!)!!
                Log.d("dowrite1", "" + hex.bytesToHexString(b))
                val c: String = hex.decodeL(b)!!
                Log.d("decodel1", "" + c + "  " + hex.decodeL(b))
                if (!c.contains("9000")) {
                    Log.d("masuk error", "1")
                    throw IOException("INIT NFC FAILED")
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }.start()
    }

    fun cekBalance(): Int {
        Thread {
            setupPort(port, connection)
            Log.d("masukcekbal", "1")
            Thread {
                if (modeOtg && !readidContinue) {
                    readid()
                }
            }.start()
        }.start()
        return 0
    }

    fun sendCTL(apdu: String): ByteArray? {
//        setupPort(port, connection);
        val ret = transceive(hex.encodeL("30" + apdu.trim { it <= ' ' })!!)
        Log.d("rapdusendctl", "" + ret)
        return hex.hexStringToByteArray(ret)
    }
}