package com.example.lib.passti

import android.content.Context
import android.hardware.usb.UsbDeviceConnection
import android.nfc.NfcAdapter
import android.nfc.NfcManager
import android.util.Log
import com.example.lib.helper.Hex
import com.hoho.android.usbserial.driver.UsbSerialPort
import java.text.SimpleDateFormat
import java.util.*

class Sam {
    var ctx: Context? = null
    var readidContinue = false
    var port: UsbSerialPort? = null
    var connection: UsbDeviceConnection? = null
    var continuePurchase = false
    private var modeOtg = false
    private var nfcAdapter: NfcAdapter? = null

    private val LUMINOS_AID = "A000000571504805"
    private val APDU_SELECT_FILE = "00A4040008"
    private val APDU_READ_BALANCE = "904C000004"
    private val LUMINOS_SAM_AID = "57100DEF000001"

    val hex=Hex()

    constructor(ctx: Context?) {
        this.ctx = ctx
    }

//    fun Sam(ctx: Context?) {
//        this.ctx = ctx
//    }


    fun init(usbSerialPort: UsbSerialPort?, connection: UsbDeviceConnection?): Int {
        nfcAdapter = (ctx!!.getSystemService(Context.NFC_SERVICE) as NfcManager).defaultAdapter
        if (nfcAdapter == null) {
            modeOtg = true
        }
        readidContinue = false
        port = usbSerialPort
        this.connection = connection
        return 0
    }

    private fun readid() {
        readidContinue = true
        Thread { readIdLoop() }.start()
    }

    @Synchronized
    private fun readIdLoop() {
        Log.d("readmasuk", "2")
        val ss: String = hex.decodeL(hex.doWrite(hex.encodeL("40")!!, port!!)!!)!!
        Log.d("sss", "" + ss)
        if (ss != null && ss.contains("9000") && ss.length >= 4) {
            Log.d("readmasuk", "3")
            readidContinue = false
            purchase(1)
        }
        if (readidContinue) {
            Log.d("readmasuk", "4")
            try {
                Thread.sleep(200)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            readIdLoop()
        }
    }

    private fun purchase(i: Int) {
        continuePurchase = true
        readidContinue = false
        Thread {
            val selectFile = transceiveNFC(APDU_SELECT_FILE + LUMINOS_AID, 4, 0)
            val cardId = selectFile!!.substring(10, 24)
            Log.d(
                "PURCHASE SELECT FILE",
                "$APDU_SELECT_FILE$LUMINOS_AID -> $selectFile  cardid $cardId"
            )
            val balance = transceiveNFC(APDU_READ_BALANCE, 4, 0)
            Log.d("PURCHASE READ BALANCE", "$APDU_READ_BALANCE -> $balance")
            val timelong = System.currentTimeMillis()
            val time = currentTime(timelong)
            val send =
                "9040030118" + amtHex(i) + time + "11111111111111111111111111".toUpperCase(Locale.getDefault())
            val initPurchase = transceiveNFC(send, 4, 0)
            Log.d("PURCHASE CTL INIT", "$send -> $initPurchase")
            val selectSam = transceiveSAM("00A4040007$LUMINOS_SAM_AID")
            Log.d("PURCHASE SAM SELECT", "00A4040007$LUMINOS_SAM_AID -> $selectSam")
            val sipcmd =
                ("800200001B" + initPurchase + amtHex(i) + "00").toUpperCase(Locale.getDefault())
            var saminitPurchase = transceiveSAM(sipcmd)
            while (saminitPurchase == null || saminitPurchase.length <= 4) {
                Log.d("PURCHASE SAM INIT1", "$sipcmd -> $saminitPurchase")
                saminitPurchase = transceiveSAM(sipcmd)
                if (!continuePurchase) {
                    Log.d("masukpurchase", "11")
                }
            }
            Log.d("PURCHASE SAM INIT2", "$sipcmd -> $saminitPurchase")
            val ctlpurchase = ("9046000012" + saminitPurchase.substring(
                0,
                saminitPurchase.length - 4
            ) + "04").toUpperCase(Locale.getDefault())
            val ctlpurchaseResp = transceiveNFC(ctlpurchase, 4, 0)
            Log.d("PURCHASE CTL DO PURCHAS", "$ctlpurchase -> $ctlpurchaseResp")
            val creditSam =
                ("8004000004" + ctlpurchaseResp + "31").toUpperCase(Locale.getDefault())
            val creditSamResp = transceiveSAM(creditSam)
            Log.d("PURCHASE SAM CREDIT2", "$creditSam -> $creditSamResp")
            //                byte[] newcredit=hexStringToByteArray(creditSamResp);
//                byte[] newsaldo= Arrays.copyOfRange(newcredit)
        }.start()
    }

    fun amtHex(amt: Int): String {
        val hex = Integer.toHexString(amt)
        Log.d("amttt", "" + amt)
        Log.d("amthex", "" + hex)
        Log.d("amtpad", "" + ("00000000".substring(hex.length) + hex))
        return "00000000".substring(hex.length) + hex
    }

    private fun currentTime(time: Long): String {
        return SimpleDateFormat("yyyyMMddHHmmss").format(time)
    }

    private fun transceive(input: ByteArray): String {
        Log.d("inputtransceive", "" + hex.bytesToHexString(input))
        Log.d("masukkedecode", "1")
        return hex.decodeL(hex.doWrite(input, port!!)!!)!!
    }

    private fun transceiveNFC(input: String, minLength: Int, currentLoop: Int): String? {
        Log.d("inputt", "" + input)
        var ret: String? = null
        Log.d("masuknfc", "1")
        if (modeOtg) {
            ret = transceive(hex.encodeL("30" + input.trim { it <= ' ' })!!)
            println("rett $ret")
            Log.d("rett", "" + ret)
            if (ret != null && ret.endsWith("9000")) {
                ret = ret.substring(0, ret.length - 4)
            }
        }
        if (ret != null && ret.length >= minLength) {
            Log.d("tesmasuk", "5")
            Log.d("final ret1", "" + ret)
            return ret
        }
        return ret
    }

    private fun transceiveSAM(input: String): String {
        return transceive(hex.encodeL("31" + input.trim { it <= ' ' })!!)
    }

    fun deduct(): Int {
        readid()
        return 0
    }

    fun sendSAM(apdu: String): ByteArray? {
        val ret = transceive(hex.encodeL("31" + apdu.trim { it <= ' ' })!!)
        Log.d("rapdusendsam", "" + ret)
        return hex.hexStringToByteArray(ret)
    }
}