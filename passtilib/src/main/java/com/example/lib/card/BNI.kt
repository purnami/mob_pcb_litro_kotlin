package com.example.lib.card

import android.content.Context
import android.util.Log
import com.example.lib.helper.APDUHelper
import com.example.lib.helper.ErrorCode
import com.example.lib.helper.Hex
import com.example.lib.passti.Reader
import java.math.BigInteger

class BNI {
    var ctx: Context? = null
    var reader: Reader? = null
    private var rapdu: ByteArray? = ByteArray(256)
    var saldoAwal = 0
    var saldoAkhir = 0
    var bniCardInfo: BniCardInfo? = null

    val hex=Hex()
    val errorCode=ErrorCode()
    val apduHelper=APDUHelper()

    private val CTL_SELECT_AID = "00A4040008A000424E4910000100"
    private val SAM_SELECT_AID = "00A4040008A000424E49200001"
    private val CTL_GET_CHALLENGE = "0084000008"
    private val GET_CARD_DATA = "903203000A1201"
    private val TERM_RND_NUM = "445D30868F586AA0"

    constructor(ctx: Context?, reader: Reader?) {
        this.ctx = ctx
        this.reader = reader
        bniCardInfo = BniCardInfo()
    }

    fun getBalance(): Int {
        return saldoAwal
    }

    fun CTL_SelectAID(): Int {
        rapdu = reader!!.sendCTL(CTL_SELECT_AID)
        Log.d("rapductlselectaid", "" + hex.bytesToHexString(rapdu))
        if (rapdu == null) {
            return errorCode.ERR_NO_RESP
        }
        return if (apduHelper.CheckResult(rapdu) != 0) {
            errorCode.ERR_SW1SW2
        } else errorCode.OK
    }

    fun cekBalance(): Int {
        var result: Int = errorCode.ERR_NO_RESP
        //        result = SAM_Init();
//        if(result!=OK) {
//            return result;
//        }
        result = CTL_GetChallenge()
        if (result != errorCode.OK) {
            return errorCode.BNI_CTL_ERR_GETCHALLENGE
        }
        System.arraycopy(rapdu, 0, bniCardInfo!!.CRN, 0, 8)
        Log.d("crn", "" + hex.bytesToHexString(bniCardInfo!!.CRN))
        result = CTL_SecureReadPurse()
        if (result != errorCode.OK) {
            return errorCode.BNI_CTL_ERR_GETPURSEDATA
        }
        Log.d("rapduuu", "" + hex.bytesToHexString(rapdu))
        val balanceCard = ByteArray(4)
        System.arraycopy(rapdu, 2, balanceCard, 1, 3)
        val `in` = BigInteger(balanceCard)
        saldoAwal = `in`.toInt()
        Log.d("Saldo", "" + saldoAwal)
        return errorCode.OK
    }

    private fun CTL_SecureReadPurse(): Int {
        Log.d("apdusecurereadpurse", "" + GET_CARD_DATA + TERM_RND_NUM + "00")
        rapdu = reader!!.sendCTL(GET_CARD_DATA + TERM_RND_NUM + "00")
        Log.d("rapductlsecurereadpurse", "" + hex.bytesToHexString(rapdu))
        return if (rapdu == null) {
            errorCode.ERR_NO_RESP
        } else errorCode.OK
        //        if(APDUHelper.CheckResult(rapdu) != 0){
//            return ERR_SW1SW2;
//        }
    }

    private fun CTL_GetChallenge(): Int {
        Log.d("BNI", "CTL_GetChallenge")
        rapdu = reader!!.sendCTL(CTL_GET_CHALLENGE)
        Log.d("rapductlgetchallenge", "" + hex.bytesToHexString(rapdu))
        if (rapdu == null) {
            return errorCode.ERR_NO_RESP
        }
        if (apduHelper.CheckResult(rapdu) != 0) {
            Log.d("masukerr", "1")
            return errorCode.ERR_SW1SW2
        }
        return errorCode.OK
    }

    private fun SAM_Init(): Int {
        val result: Int
        result = SAM_SelectAID()
        return if (result != errorCode.OK) {
            errorCode.BNI_SAM_ERR_SELECTAID
        } else errorCode.OK
    }

    private fun SAM_SelectAID(): Int {
        rapdu = reader!!.sendSAM(SAM_SELECT_AID)
        Log.d("rapdusamselectaid", "" + hex.bytesToHexString(rapdu))
        if (rapdu == null) {
            return errorCode.ERR_NO_RESP
        }
        return if (apduHelper.CheckResult(rapdu) != 0) {
            errorCode.ERR_SW1SW2
        } else errorCode.OK
    }

    internal class SamInfo {
        var id: ByteArray
        var idlen: Int

        init {
            id = ByteArray(32)
            idlen = 0
        }
    }

    class BniCardInfo {
        var CRN: ByteArray
        var lenCRN: Int
        var CSN: ByteArray
        var lenCSN: Int
        var Version: Byte
        var PurseStatus: Byte
        var KeySetIdx: Byte
        var TRH: ByteArray
        var lenTRH: Int
        var eData_CRCB: ByteArray
        var expiry: ByteArray
        var lastCreditTRP: ByteArray
        var lastCreditHeader: ByteArray
        var lastTxnTRP: ByteArray
        var lastTxnRecord: ByteArray
        var lastTxnSignCert: ByteArray
        var counterdata: ByteArray
        var lastTxnCounter: ByteArray
        var SignCert: ByteArray
        private var samInfo: SamInfo
        var leneData_CRCB: Int
        var DO: Byte
        var lastTRH: ByteArray
        var lenlastTRH: Byte
        var BackupTRH: ByteArray
        var lenBackupTRH: Byte
        var julian_card: Long
        var anonym: ByteArray

        init {
            samInfo = SamInfo()
            CRN = ByteArray(8)
            lenCRN = 0
            CSN = ByteArray(8)
            lenCSN = 0
            Version = 0
            PurseStatus = 0
            KeySetIdx = 0
            TRH = ByteArray(8)
            lenTRH = 0
            eData_CRCB = ByteArray(18)
            expiry = ByteArray(2)
            lastCreditTRP = ByteArray(4)
            lastCreditHeader = ByteArray(8)
            lastTxnTRP = ByteArray(4)
            lastTxnRecord = ByteArray(16)
            lastTxnSignCert = ByteArray(8)
            counterdata = ByteArray(8)
            lastTxnCounter = ByteArray(8)
            SignCert = ByteArray(8)
            var sam: SamInfo
            leneData_CRCB = 0
            DO = 0
            lastTRH = ByteArray(8)
            lenlastTRH = 0
            BackupTRH = ByteArray(8)
            lenBackupTRH = 0
            julian_card = 0
            anonym = ByteArray(27)
        }
    }
}