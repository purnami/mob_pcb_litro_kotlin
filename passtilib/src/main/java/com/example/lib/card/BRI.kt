package com.example.lib.card

import android.content.Context
import android.util.Log
import com.example.lib.helper.APDUHelper
import com.example.lib.helper.ErrorCode
import com.example.lib.helper.Hex
import com.example.lib.passti.Reader
import java.util.*

class BRI {
    var ctx: Context? = null
    var reader: Reader? = null
    lateinit var random: ByteArray

    val hex=Hex()
    val errorCode=ErrorCode()
    val apduHelper=APDUHelper()

    constructor(ctx: Context?, reader: Reader?) {
        this.ctx = ctx
        this.reader = reader
    }



//    fun getCardNo(): ByteArray? {
//        return cardNo
//    }
//
//    fun setCardNo(cardNo: ByteArray) {
//        this.cardNo = cardNo
//    }

    lateinit var cardNo: ByteArray

//    fun getRandom(): ByteArray {
//        return random
//    }
//
//    fun setRandom(random: ByteArray) {
//        this.random = random
//    }

    private var rapdu: ByteArray? = ByteArray(256)

    var uid: ByteArray = hex.hexStringToByteArray("8804136F42815780")!!

    private val CTL_SELECT_AID_DESFIRE = "905A00000301000000"
    private val CTL_READ_CARD_HOLDER = "90BD0000070000000000000000"
    private val CTL_READ_CI_STATUS = "90BD0000070100000000000000"
    private val CTL_SELECT_AID_3000000 = "905A00000303000000"
    private val CTL_SELECT_AID_5000000 = "905A00000305000000"
    private val CTL_GET_KEY_CARD = "900A0000010000"
    private val SAM_SELECT_AID = "00A4040009A00000000000000011"
    private val UID = "04136F42815780"

    fun cekBalance(): Int {
        val random = ByteArray(8)
        var result: Int
        result = CTL_ReadCardHolderDesfire()
        if (result != errorCode.OK) {
            return errorCode.BRI_CTL_ERR_DESFIREGETCARDNO
        }
        result = CTL_ReadCIStatus()
        if (result != errorCode.OK) {
            return errorCode.BRI_CTL_ERR_DESFIREGETCARDSTATUS
        }
        result = CTL_SelectAID_5000000()
        if (result != errorCode.OK) {
            return errorCode.BRI_CTL_ERR_DESFIRESELECTAID3
        }
        result = CTL_GetKeyCard()
        if (result != errorCode.OK) {
            return errorCode.BRI_CTL_ERR_DESFIREGETKEYCARD
        }
        result = SAM_SelectAID()
        return if (result != errorCode.OK) {
            errorCode.BRI_SAM_ERR_SELECTAID
        } else 0
        //        rapdu=reader.sendSAM(SAM_SELECT_AID);
//        Log.d("rapdusamselectaid", ""+bytesToHexString(rapdu));
//        if(rapdu==null){
//            return ERR_NO_RESP;
//        }
//        rapdu=BRI_SAM_GetRandom();
//        Log.d("rapdusamgetrandom", ""+bytesToHexString(rapdu));
//        if(rapdu==null){
//            return ERR_NO_RESP;
//        }
//        rapdu=CTL_AuthDesfire(getRandom());
//        Log.d("rapduauthdesfire", ""+bytesToHexString(rapdu));
    }

    private fun SAM_SelectAID(): Int {
        rapdu = reader!!.sendSAM(SAM_SELECT_AID)
        Log.d("rapdusamselectaid", "" + hex.bytesToHexString(rapdu))
        if (rapdu == null) {
            return errorCode.ERR_NO_RESP
        }
        return if (apduHelper.CheckResult(rapdu) != 0) {
            errorCode.ERR_SW1SW2
        } else errorCode.OK
    }

    private fun CTL_GetKeyCard(): Int {
        rapdu = reader!!.sendCTL(CTL_GET_KEY_CARD)
        Log.d("rapdugetkeycard", "" + hex.bytesToHexString(rapdu))
        if (rapdu == null) {
            return errorCode.ERR_NO_RESP
        }
        if (apduHelper.CheckResult(rapdu) != 0) {
            return errorCode.ERR_SW1SW2
        }
        random = Arrays.copyOfRange(rapdu, 0, rapdu!!.size - 2)
        Log.d("random1", "" + hex.bytesToHexString(random))
        return errorCode.OK
    }

    private fun CTL_SelectAID_5000000(): Int {
        rapdu = reader!!.sendCTL(CTL_SELECT_AID_5000000)
        Log.d("rapduselectaid5000000", "" + hex.bytesToHexString(rapdu))
        if (rapdu == null) {
            return errorCode.ERR_NO_RESP
        }
        return if (apduHelper.CheckResult(rapdu) != 0) {
            errorCode.ERR_SW1SW2
        } else errorCode.OK
    }

    private fun CTL_ReadCIStatus(): Int {
        rapdu = reader!!.sendCTL(CTL_READ_CI_STATUS)
        Log.d("rapdureadcistatus", "" + hex.bytesToHexString(rapdu))
        if (rapdu == null) {
            return errorCode.ERR_NO_RESP
        }
        if (apduHelper.CheckResult(rapdu) != 0) {
            return errorCode.ERR_SW1SW2
        }
//        if (!(rapdu.get(3).equals(0x61) && rapdu!![4] == 0x61)) {
//            errorCode.BRI_CTL_ERR_APPNOTACTIVE
//        }
        return errorCode.OK
    }

    private fun CTL_ReadCardHolderDesfire(): Int {
        rapdu = reader!!.sendCTL(CTL_READ_CARD_HOLDER)
        Log.d("rapdureadcardholder", "" + hex.bytesToHexString(rapdu))
        if (rapdu == null) {
            return errorCode.ERR_NO_RESP
        }
        if (apduHelper.CheckResult(rapdu) != 0) {
            return errorCode.ERR_SW1SW2
        }
        cardNo = Arrays.copyOfRange(rapdu, 3, 11)
        //        setCardNo(Arrays.copyOfRange(rapdu, 3, 11));
        Log.d("cardNo", "" + hex.bytesToHexString(cardNo))
        return errorCode.OK
    }

    private fun CTL_AuthDesfire(random: ByteArray): ByteArray? {
        val apdu = byteArrayOf(
            0x90.toByte(),
            0xAF.toByte(), 0x00.toByte(), 0x00.toByte(), 0x10.toByte()
        )
        var idx = 0
        val authdata = ByteArray(8)
        val command = ByteArray(apdu.size + random.size + 1)
        Log.d("randommm", "" + hex.bytesToHexString(random))
        Log.d("commandd0", "" + hex.bytesToHexString(command))
        System.arraycopy(apdu, 0, command, idx, apdu.size)
        Log.d("commandd", "" + hex.bytesToHexString(command))
        idx += apdu.size
        System.arraycopy(random, 0, command, idx, random.size)
        Log.d("commandd1", "" + hex.bytesToHexString(command))
        idx += random.size
        Log.d("commandd2", "" + hex.bytesToHexString(command))
        rapdu = reader!!.sendCTL(hex.bytesToHexString(command))
        return rapdu
    }

    fun BRI_SAM_GetRandom(): ByteArray? {
        val cmd = ByteArray(50)
        val command: ByteArray
        val random = ByteArray(16)
        var idx = 0
        cmd[idx++] = 0x80.toByte()
        cmd[idx++] = 0xB0.toByte()
        cmd[idx++] = 0x00
        cmd[idx++] = 0x00
        cmd[idx++] = 0x20
        System.arraycopy(cardNo, 0, cmd, idx, cardNo.size)
        idx += cardNo.size
        System.arraycopy(uid, 0, cmd, idx, 7)
        idx += 7
        cmd[idx++] = 0xFF.toByte()
        cmd[idx++] = 0x00
        cmd[idx++] = 0x00
        cmd[idx++] = 0x03
        cmd[idx++] = 0x00
        cmd[idx++] = 0x80.toByte()
        cmd[idx++] = 0x00
        cmd[idx++] = 0x00
        cmd[idx++] = 0x00
        System.arraycopy(this.random, 0, cmd, 29, this.random.size)
        idx += this.random.size
        command = ByteArray(idx)
        System.arraycopy(cmd, 0, command, 0, idx)
        rapdu = reader!!.sendSAM(hex.bytesToHexString(command))
        System.arraycopy(rapdu, 16, random, 0, random.size)
        this.random=random
        return rapdu
    }

    fun CTL_SelectAIDDesfire(): Int {
        rapdu = reader!!.sendCTL(CTL_SELECT_AID_DESFIRE)
        Log.d("rapduselectaiddesfire", "" + hex.bytesToHexString(rapdu))
        if (rapdu == null) {
            return errorCode.BRI_CTL_ERR_SELECTAID
        }
        return if (apduHelper.CheckResult(rapdu) != 0) {
            errorCode.BRI_CTL_ERR_SELECTAID
        } else 0
    }
}