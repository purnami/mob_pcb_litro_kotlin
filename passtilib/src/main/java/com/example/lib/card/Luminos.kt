package com.example.lib.card

import android.content.Context
import android.util.Log
import com.example.lib.helper.APDUHelper
import com.example.lib.helper.ErrorCode
import com.example.lib.helper.Hex
import com.example.lib.passti.Reader
import java.math.BigInteger
import java.text.SimpleDateFormat
import java.util.*

class Luminos {
    var ctx: Context? = null
    var reader: Reader? = null
    private var rapdu: ByteArray? = ByteArray(256)
    var saldoAwal = 0
    var saldoAkhir = 0

    private val LUMINOS_AID = "A000000571504805"
    private val APDU_SELECT_FILE = "00A4040008"
    private val APDU_READ_BALANCE = "904C000004"
    private val LUMINOS_SAM_AID = "57100DEF000001"

    val hex=Hex()
    val errorCode=ErrorCode()
    val apduHelper=APDUHelper()

    constructor(ctx: Context?, reader: Reader?) {
        this.ctx = ctx
        this.reader = reader
    }

//    fun Luminos(ctx: Context?, reader: Reader?) {
//        this.ctx = ctx
//        this.reader = reader
//    }

    fun getBalance(): Int {
        return saldoAwal
    }

    fun CTL_SelectApp(): Int {
        Log.d("masukselectapp", "1")
        rapdu = reader!!.sendCTL(APDU_SELECT_FILE + LUMINOS_AID)
        Log.d("rapduselectapp", "" + hex.bytesToHexString(rapdu))
        if (rapdu == null) {
            return errorCode.LUMINOS_CTL_ERR_SELECTAPP
        }
        return if (apduHelper.CheckResult(rapdu) != 0) {
            errorCode.LUMINOS_CTL_ERR_SELECTAPP
        } else 0
    }

    fun cekBalance(): Int {
        saldoAwal = 0
        var ret: String
        //        rapdu=reader.sendCTL("40");
//        Log.d("uidd", ""+bytesToHexString(rapdu));
        rapdu = reader!!.sendCTL(APDU_READ_BALANCE)
        //        byte[] newsaldo= Arrays.copyOfRange(rapdu, rapdu[3], rapdu[7]);
        Log.d("newrapdu", "" + hex.bytesToHexString(rapdu).toString() + "   ")
        saldoAwal = if (rapdu == null) {
            return errorCode.LUMINOS_CTL_ERR_GETBALANCE
        } else {
            if (apduHelper.CheckResult(rapdu) != 0) {
                return errorCode.LUMINOS_CTL_ERR_GETBALANCE
            }
            setSaldoAwal(hex.bytesToHexString(rapdu)!!)
            //            ret = bytesToHexString(rapdu);
            //            if(ret.endsWith("9000")){
            //                ret=ret.substring(0, ret.length()-4);
            //                BigInteger in=new BigInteger(hexStringToByteArray(ret));
            //                Log.d("balance2", ""+in.toString());
            //                saldoAwal= in.intValue();
            //                Log.d("Saldo awal", ""+saldoAwal);
            //            }
        }
        return 0
    }

    private fun setSaldoAwal(ret: String): Int {
        var ret = ret
        var saldo = 0
        if (ret.endsWith("9000")) {
            ret = ret.substring(0, ret.length - 4)
            Log.d("rettt", "" + ret)
            val `in`: BigInteger = BigInteger(hex.hexStringToByteArray(ret))
            Log.d("balance2", "" + `in`.toString())
            saldo = `in`.toInt()
            Log.d("Saldo", "" + saldo)
        }
        return saldo
    }

    fun deduct(amount: Int): Int {
        saldoAwal = 0
        saldoAkhir = 0
        return payment(amount)
    }

    private fun payment(amount: Int): Int {
        var result: ByteArray?
        cekBalance()
        result = initPurchase(amount)
        Log.d("resultinitpurchase", "" + hex.bytesToHexString(result))
        val initpurchase = Arrays.copyOfRange(result, 0, result!!.size - 2)
        Log.d("initpurchase", "" + hex.bytesToHexString(initpurchase))
        if (result == null) return errorCode.ERR_NO_RESP
        result = samSelectAid()
        if (result == null) return errorCode.ERR_NO_RESP
        result = samInitPurchase(initpurchase, amount)
        val saminit = result
        if (result == null) return errorCode.ERR_NO_RESP
        result = ctlPurchase(hex.bytesToHexString(saminit)!!)
        Log.d("resultctlpurse", "" + hex.bytesToHexString(result))
        val ctlpurchase = Arrays.copyOfRange(result, 0, result.size - 2)
        if (result == null) return errorCode.ERR_NO_RESP
        result = samCredit(hex.bytesToHexString(ctlpurchase)!!)
        saldoAkhir = setSaldoAkhir(result)
        return if (result == null) errorCode.ERR_NO_RESP else errorCode.OK
    }

    private fun setSaldoAkhir(ret: ByteArray?): Int {
        var ret = ret
        var saldo = 0
        //        Arrays.copyOfRange(result[0], 8,16);
        ret = Arrays.copyOfRange(ret, 15, 19)
        Log.d("saldoakhir", "" + hex.bytesToHexString(ret))
        val `in` = BigInteger(ret)
        Log.d("balance2", "" + `in`.toString())
        saldo = `in`.toInt()
        Log.d("Saldo", "" + saldo)
        return saldo
    }

    private fun samCredit(ctlpurchase: String): ByteArray {
        val creditSam = ("8004000004" + ctlpurchase + "31").toUpperCase(Locale.getDefault())
        rapdu = reader!!.sendSAM(creditSam)
        Log.d("PURCHASE SAM CREDIT2", creditSam + " -> " + hex.bytesToHexString(rapdu))
        return rapdu!!
    }

    private fun ctlPurchase(saminit: String): ByteArray {
        val ctlpurchase =
            ("9046000012" + saminit.substring(0, saminit.length - 4) + "04").toUpperCase(
                Locale.getDefault()
            )
        rapdu = reader!!.sendCTL(ctlpurchase)
        Log.d("PURCHASE CTL DO PURCHAS", ctlpurchase + " -> " + hex.bytesToHexString(rapdu))
        return rapdu!!
    }

    fun amtHex(amt: Int): String {
        val hex = Integer.toHexString(amt)
        Log.d("amttt", "" + amt)
        Log.d("amthex", "" + hex)
        Log.d("amtpad", "" + ("00000000".substring(hex.length) + hex))
        return "00000000".substring(hex.length) + hex
    }

    private fun samInitPurchase(initpurchase: ByteArray, amount: Int): ByteArray {
//        String hex = Integer.toHexString(amount);
//        String amtHex = "0000".substring(hex.length()) + hex;
//        String sipcmd = ("800200001B" + bytesToHexString(initpurchase) + amtHex + "00").toUpperCase(Locale.getDefault());
        val sipcmd =
            ("800200001B" + hex.bytesToHexString(initpurchase).toString() + amtHex(amount) + "00").toUpperCase(
                Locale.getDefault()
            )
        rapdu = reader!!.sendSAM(sipcmd)
        Log.d("PURCHASE SAM INIT", sipcmd + " -> " + hex.bytesToHexString(rapdu))
        return rapdu!!
    }

    private fun samSelectAid(): ByteArray {
        rapdu = reader!!.sendSAM("00A4040007$LUMINOS_SAM_AID")
        Log.d(
            "PURCHASE SAM SELECT",
            "00A4040007" + LUMINOS_SAM_AID + " -> " + hex.bytesToHexString(rapdu)
        )
        return rapdu!!
    }

    private fun initPurchase(amount: Int): ByteArray? {
        val timelong = System.currentTimeMillis()
        val time = SimpleDateFormat("yyyyMMddHHmmss").format(timelong)
        val hex = Integer.toHexString(amount)
        val amtHex = "00000000".substring(hex.length) + hex
        val send =
            "9040030118" + amtHex + time + "11111111111111111111111111".toUpperCase(Locale.getDefault())
        rapdu = reader!!.sendCTL(send)
        Log.d("PURCHASE CTL INIT", send + " -> " + this.hex.bytesToHexString(rapdu))
        return rapdu
    }

    fun getDeduct(): Int {
        return saldoAkhir
    }
}