package com.example.lib.card

import android.content.Context
import android.util.Log
import com.example.lib.helper.APDUHelper
import com.example.lib.helper.ErrorCode
import com.example.lib.helper.Hex
import com.example.lib.helper.Util
import com.example.lib.passti.Reader
import java.util.*
import kotlin.experimental.and

class Mandiri {
    var ctx: Context? = null
    var reader: Reader? = null
    private var rapdu: ByteArray? = ByteArray(256)
    var saldoAwal = 0
    var saldoAkhir = 0
    var mdrCardInfo: MdrCardInfo? = null
    var lenValue = 0
    var value = ByteArray(100)
    var cardNo = ByteArray(8)

    val hex=Hex()
    val errorCode=ErrorCode()
    val apduHelper=APDUHelper()
    val util=Util()

    private val GET_CARD_INFO_JCOPBM = "00B6000032"
    private val SELECT_APP = "00A40400080000000000000001"
    private val READ_DATA = "00B300003F"
    private val GET_BALANCE = "00B500000A"

    val offset_appid = 18
    val APPID_03 = 0x03.toByte()
    val APPID_02 = 0x02.toByte()
    val APPID_83 = 0x83.toByte()

    constructor(ctx: Context?, reader: Reader?) {
        this.ctx = ctx
        this.reader = reader
        mdrCardInfo = MdrCardInfo()
    }

    fun CTL_Init(): Int {
        var result = CTL_ReadCardInfo_JCOPBM()
        Log.d("mandiri result", "" + result)
        if (result != errorCode.OK) {
            Log.d("mandiri", "result !=OK")
            if (result == errorCode.ERR_SW1SW2) {
                Log.d("Mandiri", "Non-JCOPBM")
                result = CTL_SelectApp()
                if (result != errorCode.OK) return errorCode.MDR_CTL_ERR_SELECTAPP
                result = CTL_ReadData()
                if (result != errorCode.OK) return errorCode.MDR_CTL_ERR_READDATA
                Log.d("Mandiri", "standard")
                mdrCardInfo!!.JCOP = false
                //return OK;
            } else return result
        } else {
            Log.d("Mandiri", "JCOPBM")
            mdrCardInfo!!.JCOP = true
            //return OK;
        }
        return result
    }

    private fun CTL_ReadData(): Int {
        rapdu = reader!!.sendCTL(READ_DATA)
        Log.d("rapductlreaddata", "" + hex.bytesToHexString(rapdu))
        if (rapdu == null) {
            return errorCode.ERR_NO_RESP
        }
        if (rapdu!!.size < 2) return errorCode.ERR_NO_RESP
        if (apduHelper.CheckResult(rapdu) != 0) return errorCode.ERR_SW1SW2
        mdrCardInfo!!.appId = rapdu!![offset_appid]
        Log.d("mdrCardInfo.appId", "" + hex.byteToHexString(mdrCardInfo!!.appId))
        System.arraycopy(rapdu, 0, cardNo, 0, 8)
        Log.d("Card No", "" + hex.bytesToHexString(cardNo))
        System.arraycopy(rapdu, 0, value, 0, 63)
        lenValue = 63
        return errorCode.OK
    }

    private fun CTL_SelectApp(): Int {
        rapdu = reader!!.sendCTL(SELECT_APP)
        Log.d("rapductlselectapp", "" + hex.bytesToHexString(rapdu))
        if (rapdu == null) {
            return errorCode.ERR_NO_RESP
        }
        if (rapdu!!.size < 2) return errorCode.ERR_NO_RESP
        return if (apduHelper.CheckResult(rapdu) != 0) errorCode.ERR_SW1SW2 else errorCode.OK
    }

    private fun CTL_ReadCardInfo_JCOPBM(): Int {
        rapdu = null
        rapdu = reader!!.sendCTL(GET_CARD_INFO_JCOPBM)
        Log.d("rapductlgetcardinfo", "" + hex.bytesToHexString(rapdu))
        if (rapdu == null) {
            return errorCode.ERR_NO_RESP
        }
        if (rapdu!!.size < 2) return errorCode.ERR_NO_RESP
        if (apduHelper.CheckResult(rapdu) != 0) return errorCode.ERR_SW1SW2
        mdrCardInfo!!.appId = rapdu!![offset_appid]
        Log.d("Mandiri", "ctl_mdr.appid " + hex.byteToHexString(mdrCardInfo!!.appId))
        lenValue = rapdu!!.size - 2
        Log.d("lenValue", "" + lenValue)
        System.arraycopy(rapdu, 0, value, 0, lenValue)
        Log.d("value", "" + hex.bytesToHexString(value))
        System.arraycopy(rapdu, 0, cardNo, 0, cardNo.size)
        return errorCode.OK
    }

    fun getBalance(): Int {
        return saldoAwal
    }

    fun cekBalance(): Int {
        CTL_Init()
        val deviceTime: ByteArray = hex.hexStringToByteArray(util.dateNow("ddMMyyHHmmss")!!)!!
        Log.d("deviceTime", "" + hex.bytesToHexString(deviceTime))
        Log.d("appId", "" + hex.byteToHexString(mdrCardInfo!!.appId))
        when (mdrCardInfo!!.appId) {
            APPID_83 -> return CTL_GetBalance()
            APPID_03 -> return CTL_GetBalance()
        }
        return errorCode.OK
    }

    private fun CTL_GetBalance(): Int {
        rapdu = null
        rapdu = reader!!.sendCTL(GET_BALANCE)
        Log.d("rapductlgetbalance", "" + hex.bytesToHexString(rapdu))
        if (rapdu == null) {
            return errorCode.MDR_CTL_ERR_GETBALANCE
        }
        if (rapdu!!.size < 2) return errorCode.MDR_CTL_ERR_GETBALANCE
        if (apduHelper.CheckResult(rapdu) != 0) return errorCode.MDR_CTL_ERR_GETBALANCE
        val saldo = Arrays.copyOfRange(rapdu, 0, 4)
        saldoAwal = toDec(saldo).toString().toInt()
        Log.d("Saldo", "" + saldoAwal)

        return errorCode.OK
    }

    class MdrCardInfo {
        var appId: Byte = 0
        var JCOP = false
    }

    private fun toDec(bytes: ByteArray): Long {
        var result: Long = 0
        var factor: Long = 1
        for (i in bytes.indices) {
            val value: Byte = bytes[i] and 0xffL.toByte()
            result += value * factor
            factor *= 256L
        }
        return result
    }
}