package com.example.emoneyreader_kotlin

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.lib.helper.ErrorCode
import com.example.lib.passti.Reader
import com.example.lib.passti.STIUtility
import java.text.NumberFormat
import java.util.*

class MainActivity : AppCompatActivity() {
    var tv: TextView? = null
    var initReader: Button? = null
    var initPower: Button? = null
    var cekBalance: Button? = null
    var deduct: Button? = null
    var spinnerCard: Spinner? = null
    var nominal: EditText? = null

    var sti: STIUtility? = null
    var reader: Reader?= null

    var card = 0

    val errorCode=ErrorCode()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tv = findViewById(R.id.tv)
        initReader = findViewById(R.id.initReader)
        initPower = findViewById(R.id.initPower)
        cekBalance = findViewById(R.id.cekbalance)
        deduct = findViewById(R.id.deduct)
        nominal = findViewById(R.id.nominal)
        spinnerCard = findViewById(R.id.spinnerCard)
        spinnerCard!!.setOnItemSelectedListener(cardSelected)


//        reader=new Reader(getApplicationContext());
        initReader!!.setOnClickListener(clickInitReader)
        initPower!!.setOnClickListener(clickinitPower)
        cekBalance!!.setOnClickListener(clickCekBalance)
        deduct!!.setOnClickListener(clickDeduct)
    }
    var cardSelected: AdapterView.OnItemSelectedListener =
        object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                when (position) {
                    0 -> card = 1
                    1 -> card = 2
                    2 -> card = 3
                    3 -> card = 4
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

    var clickInitReader = View.OnClickListener {
        Log.d("unknown", ""+errorCode!!.ERR_UNKNOWN)
        var result: Int = errorCode!!.ERR_UNKNOWN
        if (sti == null) {
            sti = STIUtility(applicationContext)
            result = sti!!.initReader()
        }
    }

    var clickinitPower = View.OnClickListener {
        Log.d("cardd", "" + card)
        var result: Int = errorCode!!.ERR_UNKNOWN
        result = sti!!.initPower(card)
    }

    var clickCekBalance = View.OnClickListener {
        try {
            Log.d("clickcekbalance", "1")
            var result: Int = errorCode!!.ERR_UNKNOWN
            result = sti!!.cekBalance()
            Log.d("resultcekbal1", "" + result)
            if (result == errorCode!!.OK) {
                val locale = Locale("id", "ID")
                val format = NumberFormat.getCurrencyInstance(locale)
                showText("Saldo : " + format.format(sti!!.getBalance()))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    var clickDeduct = View.OnClickListener {
        Log.d("clickcekdeduct", "1")
        var result: Int = errorCode!!.ERR_UNKNOWN
        val i = nominal!!.text.toString()
        result = sti!!.deduct(i.toInt())
        Log.d("resultdeduct1", "" + result)
        if (result == errorCode!!.OK) {
            val locale = Locale("id", "ID")
            val format = NumberFormat.getCurrencyInstance(locale)
            showText(
                """
                      Saldo Awal : ${format.format(sti!!.getBalance())}
                      Saldo Akhir : ${format.format(sti!!.getDeduct())}
                      """.trimIndent()
            )
        }
    }

    private fun showText(text: String) {
        object : Thread() {
            override fun run() {
                try {
                    runOnUiThread { tv!!.text = text }
                    sleep(300)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }.start()
    }
}